%% Problem specifications

m0 = 1;
m1 = .5;
m2 = .5;
l1 = 1;
l2 = .5;
g=9.82;

%% Define as functions

M = @(theta1, theta2) [(m0 + m1 + m2) (m1+m2)*l1*cos(theta1) m2*l2*cos(theta2)
    (m1+m2)*l1*cos(theta1) 1^2*(m1+m2) m2*l1*l2*cos(theta1-theta2)
    m2*l2*cos(theta2) m2*l1*l2*cos(theta1-theta2) m2*l2^2];

F = @(theta1, theta2, theta1dot, theta2dot) [(m1+m2)*l1*theta1dot^2*sin(theta1)+m2*l2*theta2dot^2*sin(theta2)
    (m1+m2)*l1*g*sin(theta1)-m2*l1*l2*theta2dot^2*sin(theta1-theta2)
    m2*l2*g*sin(theta2)+m2*l1*l2*theta1dot^2*sin(theta1-theta2)];
B = [1 0 0]';
%Potential energy
P = @(theta1, theta2) l1*(m1+m2)*cos(theta1)+l2*m2*cos(theta2);
E = @(x,theta1, theta2,xdot, theta1dot, theta2dot) [xdot, theta1dot, theta2dot]*M(theta1,theta2)*[xdot, theta1dot, theta2dot]' + P(theta1,theta2);
%% Controller u

%Desired energy
Er = P(0,0);


maxVal = 0;

for v1 = (-pi):.01:pi;
    for v2 = (-pi):.01:pi;
        val = (Er-P(v1,v2))/(B'*(M(v1,v2)\B));
        if (val>maxVal)
            maxVal = val;
            a1 = v1;
            a2 = v2;
        end
    end
end
kD = 1.1*maxVal;
k = 20;
kP = 12;
%% Controller
f = @(x, theta1, theta2, xdot, theta1dot, theta2dot)  kD*B'*(M(theta1,theta2)\(F(theta1,theta2,theta1dot, theta2dot))- kP*x - k*xdot)/(E(x, theta1, theta2, xdot, theta1dot, theta2dot) - Er + kD*B'*(M(theta1,theta2)\B));
%% Statespace model

Fhat = @(theta1, theta2, theta1dot, theta2dot) M(theta1, theta2)\F(theta1, theta2, theta1dot, theta2dot);

G = [0 0 0 1 0 0]';

A = @ (x1, x2, x3, x4, x5, x6) [x4;x5;x6;Fhat(x2,x3,x5,x6)];

%% Use this to test equillibrium points!
Fhat(0,0,0,0)
%% Time for simulink?

IC1 = 0;
IC2 = 0.1;
IC3 = 0.1;
IC4 = 0;
IC5 = 0;
IC6 = 0;

%% gmm

fcn(0,pi,pi,0,0,0)


%% PLOTTING
subplot(1,2,1)
plot(tout, simout.data(:, 2));
subplot(1,2,2)
plot(tout, simout.data(:, 3))
