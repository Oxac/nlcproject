/* Include files */

#include "plant_sfun.h"
#include "c1_plant.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "plant_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c1_debug_family_names[30] = { "m0", "m1", "m2", "l1", "l2",
  "g", "Er", "kD", "k", "kP", "M", "F", "P", "E", "B", "u", "z", "G", "a",
  "epsilon", "nargin", "nargout", "x1", "x2", "x3", "x4", "x5", "x6", "f", "out"
};

/* Function Declarations */
static void initialize_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void initialize_params_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void enable_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void disable_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void c1_update_debugger_state_c1_plant(SFc1_plantInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c1_plant(SFc1_plantInstanceStruct
  *chartInstance);
static void set_sim_state_c1_plant(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_st);
static void finalize_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void sf_gateway_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void mdl_start_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void c1_chartstep_c1_plant(SFc1_plantInstanceStruct *chartInstance);
static void initSimStructsc1_plant(SFc1_plantInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber);
static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData);
static real_T c1_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_b_out, const char_T *c1_identifier);
static real_T c1_b_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_c_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_b_f, const char_T *c1_identifier, real_T c1_y[6]);
static void c1_d_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[6]);
static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_e_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[3]);
static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static void c1_f_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[9]);
static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static real_T c1_cos(SFc1_plantInstanceStruct *chartInstance, real_T c1_x);
static real_T c1_mpower(SFc1_plantInstanceStruct *chartInstance, real_T c1_a);
static void c1_scalarEg(SFc1_plantInstanceStruct *chartInstance);
static void c1_dimagree(SFc1_plantInstanceStruct *chartInstance);
static void c1_error(SFc1_plantInstanceStruct *chartInstance);
static real_T c1_sin(SFc1_plantInstanceStruct *chartInstance, real_T c1_x);
static void c1_b_scalarEg(SFc1_plantInstanceStruct *chartInstance);
static void c1_c_scalarEg(SFc1_plantInstanceStruct *chartInstance);
static void c1_d_scalarEg(SFc1_plantInstanceStruct *chartInstance);
static void c1_mldivide(SFc1_plantInstanceStruct *chartInstance, real_T c1_A[9],
  real_T c1_B[3], real_T c1_Y[3]);
static real_T c1_abs(SFc1_plantInstanceStruct *chartInstance, real_T c1_x);
static void c1_warning(SFc1_plantInstanceStruct *chartInstance);
static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData);
static int32_T c1_g_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData);
static uint8_T c1_h_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_b_is_active_c1_plant, const char_T *c1_identifier);
static uint8_T c1_i_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId);
static void c1_b_cos(SFc1_plantInstanceStruct *chartInstance, real_T *c1_x);
static void c1_b_sin(SFc1_plantInstanceStruct *chartInstance, real_T *c1_x);
static void init_dsm_address_info(SFc1_plantInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc1_plantInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc1_plant(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c1_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c1_is_active_c1_plant = 0U;
}

static void initialize_params_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c1_update_debugger_state_c1_plant(SFc1_plantInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c1_plant(SFc1_plantInstanceStruct
  *chartInstance)
{
  const mxArray *c1_st;
  const mxArray *c1_y = NULL;
  const mxArray *c1_b_y = NULL;
  real_T c1_hoistedGlobal;
  real_T c1_u;
  const mxArray *c1_c_y = NULL;
  uint8_T c1_b_hoistedGlobal;
  uint8_T c1_b_u;
  const mxArray *c1_d_y = NULL;
  c1_st = NULL;
  c1_st = NULL;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_createcellmatrix(3, 1), false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", *chartInstance->c1_f, 0, 0U, 1U, 0U,
    1, 6), false);
  sf_mex_setcell(c1_y, 0, c1_b_y);
  c1_hoistedGlobal = *chartInstance->c1_out;
  c1_u = c1_hoistedGlobal;
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c1_y, 1, c1_c_y);
  c1_b_hoistedGlobal = chartInstance->c1_is_active_c1_plant;
  c1_b_u = c1_b_hoistedGlobal;
  c1_d_y = NULL;
  sf_mex_assign(&c1_d_y, sf_mex_create("y", &c1_b_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c1_y, 2, c1_d_y);
  sf_mex_assign(&c1_st, c1_y, false);
  return c1_st;
}

static void set_sim_state_c1_plant(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_st)
{
  const mxArray *c1_u;
  real_T c1_dv0[6];
  int32_T c1_i0;
  chartInstance->c1_doneDoubleBufferReInit = true;
  c1_u = sf_mex_dup(c1_st);
  c1_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("f", c1_u, 0)),
                        "f", c1_dv0);
  for (c1_i0 = 0; c1_i0 < 6; c1_i0++) {
    (*chartInstance->c1_f)[c1_i0] = c1_dv0[c1_i0];
  }

  *chartInstance->c1_out = c1_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("out", c1_u, 1)), "out");
  chartInstance->c1_is_active_c1_plant = c1_h_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_active_c1_plant", c1_u, 2)),
    "is_active_c1_plant");
  sf_mex_destroy(&c1_u);
  c1_update_debugger_state_c1_plant(chartInstance);
  sf_mex_destroy(&c1_st);
}

static void finalize_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  int32_T c1_i1;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_x6, 5U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_x5, 4U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_x4, 3U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_x3, 2U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_x2, 1U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_x1, 0U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
  chartInstance->c1_sfEvent = CALL_EVENT;
  c1_chartstep_c1_plant(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_plantMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c1_i1 = 0; c1_i1 < 6; c1_i1++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c1_f)[c1_i1], 6U, 1U, 0U,
                          chartInstance->c1_sfEvent, false);
  }

  _SFD_DATA_RANGE_CHECK(*chartInstance->c1_out, 7U, 1U, 0U,
                        chartInstance->c1_sfEvent, false);
}

static void mdl_start_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_chartstep_c1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  real_T c1_hoistedGlobal;
  real_T c1_b_hoistedGlobal;
  real_T c1_c_hoistedGlobal;
  real_T c1_d_hoistedGlobal;
  real_T c1_e_hoistedGlobal;
  real_T c1_f_hoistedGlobal;
  real_T c1_b_x1;
  real_T c1_b_x2;
  real_T c1_b_x3;
  real_T c1_b_x4;
  real_T c1_b_x5;
  real_T c1_b_x6;
  uint32_T c1_debug_family_var_map[30];
  real_T c1_m0;
  real_T c1_m1;
  real_T c1_m2;
  real_T c1_l1;
  real_T c1_l2;
  real_T c1_g;
  real_T c1_Er;
  real_T c1_kD;
  real_T c1_k;
  real_T c1_kP;
  real_T c1_M[9];
  real_T c1_F[3];
  real_T c1_P;
  real_T c1_E;
  real_T c1_B[3];
  real_T c1_u;
  real_T c1_z[3];
  real_T c1_G[3];
  real_T c1_a;
  real_T c1_epsilon;
  real_T c1_b_u[3];
  real_T c1_nargin = 6.0;
  real_T c1_nargout = 2.0;
  real_T c1_b_f[6];
  real_T c1_b_out;
  real_T c1_d0;
  real_T c1_d1;
  real_T c1_d2;
  real_T c1_d3;
  real_T c1_d4;
  real_T c1_d5;
  real_T c1_d6;
  real_T c1_d7;
  real_T c1_d8;
  real_T c1_d9;
  real_T c1_d10;
  real_T c1_d11;
  real_T c1_d12;
  real_T c1_d13;
  real_T c1_d14;
  real_T c1_d15;
  real_T c1_d16;
  real_T c1_d17;
  real_T c1_b_a[3];
  int32_T c1_i2;
  int32_T c1_i3;
  real_T c1_b[9];
  int32_T c1_i4;
  real_T c1_c_x4[3];
  real_T c1_y[3];
  int32_T c1_i5;
  int32_T c1_i6;
  real_T c1_b_y;
  real_T c1_b_b[3];
  int32_T c1_b_k;
  int32_T c1_c_k;
  int32_T c1_i7;
  static real_T c1_dv1[3] = { 1.0, 0.0, 0.0 };

  int32_T c1_i8;
  int32_T c1_i9;
  real_T c1_b_M[9];
  real_T c1_b_F[3];
  real_T c1_c_y;
  int32_T c1_d_k;
  int32_T c1_i10;
  int32_T c1_e_k;
  static real_T c1_dv2[3] = { 40.0, 0.0, 0.0 };

  int32_T c1_i11;
  real_T c1_c_M[9];
  real_T c1_dv3[3];
  real_T c1_d_y;
  int32_T c1_f_k;
  real_T c1_A;
  int32_T c1_g_k;
  real_T c1_b_B;
  real_T c1_x;
  real_T c1_e_y;
  real_T c1_b_x;
  real_T c1_f_y;
  int32_T c1_i12;
  int32_T c1_i13;
  real_T c1_d_M[9];
  real_T c1_dv4[3];
  real_T c1_c_b;
  int32_T c1_i14;
  int32_T c1_i15;
  int32_T c1_i16;
  int32_T c1_i17;
  real_T c1_e_M[9];
  real_T c1_dv5[3];
  real_T c1_dv6[3];
  int32_T c1_i18;
  int32_T c1_i19;
  int32_T c1_i20;
  real_T c1_g_y;
  int32_T c1_h_k;
  int32_T c1_i_k;
  int32_T c1_i21;
  int32_T c1_i22;
  int32_T c1_i23;
  int32_T c1_i24;
  int32_T c1_i25;
  int32_T c1_i26;
  int32_T c1_i27;
  int32_T c1_i28;
  int32_T c1_i29;
  real_T c1_f_M[9];
  int32_T c1_i30;
  real_T c1_g_M[9];
  real_T c1_c_F[3];
  real_T c1_dv7[3];
  real_T c1_d_F[3];
  int32_T c1_i31;
  int32_T c1_i32;
  real_T c1_h_y;
  real_T c1_i_y;
  int32_T c1_j_k;
  int32_T c1_k_k;
  real_T c1_b_A;
  int32_T c1_l_k;
  int32_T c1_i33;
  int32_T c1_m_k;
  real_T c1_c_x;
  real_T c1_d_x;
  real_T c1_j_y;
  int32_T c1_i34;
  int32_T c1_i35;
  real_T c1_k_y;
  int32_T c1_i36;
  int32_T c1_n_k;
  real_T c1_l_y;
  real_T c1_c_A;
  int32_T c1_o_k;
  int32_T c1_p_k;
  real_T c1_c_B;
  real_T c1_e_x;
  real_T c1_m_y;
  real_T c1_f_x;
  int32_T c1_q_k;
  real_T c1_g_x;
  real_T c1_h_x;
  real_T c1_n_y;
  int32_T c1_i37;
  real_T c1_d_b;
  int32_T c1_i38;
  int32_T c1_i39;
  real_T c1_c_u;
  int32_T c1_i40;
  const mxArray *c1_o_y = NULL;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
  c1_hoistedGlobal = *chartInstance->c1_x1;
  c1_b_hoistedGlobal = *chartInstance->c1_x2;
  c1_c_hoistedGlobal = *chartInstance->c1_x3;
  c1_d_hoistedGlobal = *chartInstance->c1_x4;
  c1_e_hoistedGlobal = *chartInstance->c1_x5;
  c1_f_hoistedGlobal = *chartInstance->c1_x6;
  c1_b_x1 = c1_hoistedGlobal;
  c1_b_x2 = c1_b_hoistedGlobal;
  c1_b_x3 = c1_c_hoistedGlobal;
  c1_b_x4 = c1_d_hoistedGlobal;
  c1_b_x5 = c1_e_hoistedGlobal;
  c1_b_x6 = c1_f_hoistedGlobal;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 30U, 31U, c1_debug_family_names,
    c1_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_m0, 0U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_m1, 1U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_m2, 2U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_l1, 3U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_l2, 4U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_g, 5U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_Er, 6U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_kD, 7U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_k, 8U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_kP, 9U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_M, 10U, c1_d_sf_marshallOut,
    c1_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_F, 11U, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_P, 12U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_E, 13U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c1_B, 14U, c1_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_u, MAX_uint32_T, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_z, 16U, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_G, 17U, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_a, 18U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_epsilon, 19U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_b_u, MAX_uint32_T, c1_c_sf_marshallOut,
    c1_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargin, 20U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_nargout, 21U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_b_x1, 22U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_b_x2, 23U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_b_x3, 24U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_b_x4, 25U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_b_x5, 26U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c1_b_x6, 27U, c1_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c1_b_f, 28U, c1_b_sf_marshallOut,
    c1_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c1_b_out, 29U, c1_sf_marshallOut,
    c1_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 3);
  c1_m0 = 1.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 4);
  c1_m1 = 0.5;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 5);
  c1_m2 = 0.5;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 6);
  c1_l1 = 1.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 7);
  c1_l2 = 1.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 8);
  c1_g = 9.82;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 9);
  c1_Er = 1.5;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 10);
  c1_kD = 40.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 11);
  c1_k = 30.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 12);
  c1_kP = 40.0;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 14);
  c1_d0 = c1_b_x2;
  c1_b_cos(chartInstance, &c1_d0);
  c1_d1 = c1_b_x3;
  c1_b_cos(chartInstance, &c1_d1);
  c1_d2 = c1_b_x2;
  c1_b_cos(chartInstance, &c1_d2);
  c1_d3 = c1_b_x2 - c1_b_x3;
  c1_b_cos(chartInstance, &c1_d3);
  c1_d4 = c1_b_x3;
  c1_b_cos(chartInstance, &c1_d4);
  c1_d5 = c1_b_x2 - c1_b_x3;
  c1_b_cos(chartInstance, &c1_d5);
  c1_M[0] = 2.0;
  c1_M[3] = (c1_m1 + c1_m2) * c1_l1 * c1_d0;
  c1_M[6] = c1_m2 * c1_l2 * c1_d1;
  c1_M[1] = (c1_m1 + c1_m2) * c1_l1 * c1_d2;
  c1_M[4] = 1.0;
  c1_M[7] = c1_m2 * c1_l1 * c1_l2 * c1_d3;
  c1_M[2] = c1_m2 * c1_l2 * c1_d4;
  c1_M[5] = c1_m2 * c1_l1 * c1_l2 * c1_d5;
  c1_M[8] = 0.5;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 18);
  c1_d6 = c1_b_x2;
  c1_b_sin(chartInstance, &c1_d6);
  c1_d7 = c1_b_x3;
  c1_b_sin(chartInstance, &c1_d7);
  c1_d8 = c1_b_x2;
  c1_b_sin(chartInstance, &c1_d8);
  c1_d9 = c1_b_x2 - c1_b_x3;
  c1_b_sin(chartInstance, &c1_d9);
  c1_d10 = c1_b_x3;
  c1_b_sin(chartInstance, &c1_d10);
  c1_d11 = c1_b_x2 - c1_b_x3;
  c1_b_sin(chartInstance, &c1_d11);
  c1_d12 = c1_mpower(chartInstance, c1_b_x5);
  c1_d13 = c1_mpower(chartInstance, c1_b_x6);
  c1_d14 = c1_mpower(chartInstance, c1_b_x6);
  c1_d15 = c1_mpower(chartInstance, c1_b_x5);
  c1_F[0] = (c1_m1 + c1_m2) * c1_l1 * c1_d12 * c1_d6 + c1_m2 * c1_l2 * c1_d13 *
    c1_d7;
  c1_F[1] = (c1_m1 + c1_m2) * c1_l1 * c1_g * c1_d8 - c1_m2 * c1_l1 * c1_l2 *
    c1_d14 * c1_d9;
  c1_F[2] = c1_m2 * c1_l2 * c1_g * c1_d10 + c1_m2 * c1_l1 * c1_l2 * c1_d15 *
    c1_d11;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 22);
  c1_d16 = c1_b_x2;
  c1_b_cos(chartInstance, &c1_d16);
  c1_d17 = c1_b_x3;
  c1_b_cos(chartInstance, &c1_d17);
  c1_P = c1_l1 * (c1_m1 + c1_m2) * c1_d16 + c1_l2 * c1_m2 * c1_d17;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 23);
  c1_b_a[0] = c1_b_x4;
  c1_b_a[1] = c1_b_x5;
  c1_b_a[2] = c1_b_x6;
  for (c1_i2 = 0; c1_i2 < 9; c1_i2++) {
    c1_b[c1_i2] = c1_M[c1_i2];
  }

  c1_i3 = 0;
  for (c1_i4 = 0; c1_i4 < 3; c1_i4++) {
    c1_y[c1_i4] = 0.0;
    for (c1_i5 = 0; c1_i5 < 3; c1_i5++) {
      c1_y[c1_i4] += c1_b_a[c1_i5] * c1_b[c1_i5 + c1_i3];
    }

    c1_i3 += 3;
  }

  c1_c_x4[0] = c1_b_x4;
  c1_c_x4[1] = c1_b_x5;
  c1_c_x4[2] = c1_b_x6;
  for (c1_i6 = 0; c1_i6 < 3; c1_i6++) {
    c1_b_b[c1_i6] = c1_c_x4[c1_i6];
  }

  c1_b_y = 0.0;
  for (c1_b_k = 1; c1_b_k < 4; c1_b_k++) {
    c1_c_k = c1_b_k - 1;
    c1_b_y += c1_y[c1_c_k] * c1_b_b[c1_c_k];
  }

  c1_E = c1_b_y + c1_P;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 25);
  for (c1_i7 = 0; c1_i7 < 3; c1_i7++) {
    c1_B[c1_i7] = c1_dv1[c1_i7];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 27);
  for (c1_i8 = 0; c1_i8 < 9; c1_i8++) {
    c1_b_M[c1_i8] = c1_M[c1_i8];
  }

  for (c1_i9 = 0; c1_i9 < 3; c1_i9++) {
    c1_b_F[c1_i9] = -c1_F[c1_i9];
  }

  c1_mldivide(chartInstance, c1_b_M, c1_b_F, c1_b_b);
  c1_c_y = 0.0;
  for (c1_d_k = 1; c1_d_k < 4; c1_d_k++) {
    c1_e_k = c1_d_k - 1;
    c1_c_y += c1_dv2[c1_e_k] * c1_b_b[c1_e_k];
  }

  for (c1_i10 = 0; c1_i10 < 9; c1_i10++) {
    c1_c_M[c1_i10] = c1_M[c1_i10];
  }

  for (c1_i11 = 0; c1_i11 < 3; c1_i11++) {
    c1_dv3[c1_i11] = c1_dv1[c1_i11];
  }

  c1_mldivide(chartInstance, c1_c_M, c1_dv3, c1_b_b);
  c1_d_y = 0.0;
  for (c1_f_k = 1; c1_f_k < 4; c1_f_k++) {
    c1_g_k = c1_f_k - 1;
    c1_d_y += c1_dv2[c1_g_k] * c1_b_b[c1_g_k];
  }

  c1_A = (c1_c_y - c1_kP * c1_b_x1) - c1_k * c1_b_x4;
  c1_b_B = (c1_E - c1_Er) + c1_d_y;
  c1_x = c1_A;
  c1_e_y = c1_b_B;
  c1_b_x = c1_x;
  c1_f_y = c1_e_y;
  c1_u = c1_b_x / c1_f_y;
  _SFD_SYMBOL_SWITCH(15U, 15U);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 29);
  for (c1_i12 = 0; c1_i12 < 9; c1_i12++) {
    c1_d_M[c1_i12] = c1_M[c1_i12];
  }

  for (c1_i13 = 0; c1_i13 < 3; c1_i13++) {
    c1_dv4[c1_i13] = c1_dv1[c1_i13];
  }

  c1_mldivide(chartInstance, c1_d_M, c1_dv4, c1_b_b);
  c1_c_b = c1_u;
  for (c1_i14 = 0; c1_i14 < 3; c1_i14++) {
    c1_b_u[c1_i14] = c1_b_b[c1_i14] * c1_c_b;
  }

  _SFD_SYMBOL_SWITCH(15U, 20U);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 30);
  c1_b_f[0] = 0.0;
  c1_b_f[1] = 0.0;
  c1_b_f[2] = 0.0;
  for (c1_i15 = 0; c1_i15 < 3; c1_i15++) {
    c1_b_f[c1_i15 + 3] = c1_b_u[c1_i15];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 32);
  c1_z[0] = c1_b_x1 + c1_b_x4;
  c1_z[1] = c1_b_x2 + c1_b_x5;
  c1_z[2] = c1_b_x3 + c1_b_x6;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 33);
  for (c1_i16 = 0; c1_i16 < 9; c1_i16++) {
    c1_e_M[c1_i16] = c1_M[c1_i16];
  }

  for (c1_i17 = 0; c1_i17 < 3; c1_i17++) {
    c1_dv5[c1_i17] = c1_dv1[c1_i17];
  }

  c1_mldivide(chartInstance, c1_e_M, c1_dv5, c1_dv6);
  for (c1_i18 = 0; c1_i18 < 3; c1_i18++) {
    c1_G[c1_i18] = c1_dv6[c1_i18];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 34);
  for (c1_i19 = 0; c1_i19 < 3; c1_i19++) {
    c1_b_a[c1_i19] = c1_z[c1_i19];
  }

  for (c1_i20 = 0; c1_i20 < 3; c1_i20++) {
    c1_b_b[c1_i20] = c1_G[c1_i20];
  }

  c1_g_y = 0.0;
  for (c1_h_k = 1; c1_h_k < 4; c1_h_k++) {
    c1_i_k = c1_h_k - 1;
    c1_g_y += c1_b_a[c1_i_k] * c1_b_b[c1_i_k];
  }

  c1_a = c1_abs(chartInstance, c1_g_y);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 35);
  c1_epsilon = 0.01;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 36);
  if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c1_a, c1_epsilon, -1, 4U,
        c1_a > c1_epsilon))) {
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 37);
    for (c1_i22 = 0; c1_i22 < 3; c1_i22++) {
      c1_b_b[c1_i22] = c1_z[c1_i22];
    }

    for (c1_i24 = 0; c1_i24 < 3; c1_i24++) {
      c1_b_b[c1_i24] *= 2.0;
    }

    for (c1_i26 = 0; c1_i26 < 3; c1_i26++) {
      c1_b_a[c1_i26] = -c1_z[c1_i26];
    }

    for (c1_i28 = 0; c1_i28 < 9; c1_i28++) {
      c1_g_M[c1_i28] = c1_M[c1_i28];
    }

    for (c1_i30 = 0; c1_i30 < 3; c1_i30++) {
      c1_d_F[c1_i30] = c1_F[c1_i30];
    }

    c1_mldivide(chartInstance, c1_g_M, c1_d_F, c1_dv7);
    for (c1_i32 = 0; c1_i32 < 3; c1_i32++) {
      c1_b_b[c1_i32] += c1_dv7[c1_i32];
    }

    c1_i_y = 0.0;
    for (c1_k_k = 1; c1_k_k < 4; c1_k_k++) {
      c1_m_k = c1_k_k - 1;
      c1_i_y += c1_b_a[c1_m_k] * c1_b_b[c1_m_k];
    }

    for (c1_i33 = 0; c1_i33 < 3; c1_i33++) {
      c1_b_a[c1_i33] = c1_z[c1_i33];
    }

    for (c1_i34 = 0; c1_i34 < 3; c1_i34++) {
      c1_b_b[c1_i34] = c1_G[c1_i34];
    }

    c1_k_y = 0.0;
    for (c1_n_k = 1; c1_n_k < 4; c1_n_k++) {
      c1_o_k = c1_n_k - 1;
      c1_k_y += c1_b_a[c1_o_k] * c1_b_b[c1_o_k];
    }

    c1_c_A = c1_i_y;
    c1_c_B = c1_k_y;
    c1_e_x = c1_c_A;
    c1_m_y = c1_c_B;
    c1_g_x = c1_e_x;
    c1_n_y = c1_m_y;
    c1_u = c1_g_x / c1_n_y;
    _SFD_SYMBOL_SWITCH(15U, 15U);
  } else {
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 39);
    for (c1_i21 = 0; c1_i21 < 3; c1_i21++) {
      c1_b_b[c1_i21] = c1_z[c1_i21];
    }

    for (c1_i23 = 0; c1_i23 < 3; c1_i23++) {
      c1_b_b[c1_i23] *= 2.0;
    }

    for (c1_i25 = 0; c1_i25 < 3; c1_i25++) {
      c1_b_a[c1_i25] = -c1_z[c1_i25];
    }

    for (c1_i27 = 0; c1_i27 < 9; c1_i27++) {
      c1_f_M[c1_i27] = c1_M[c1_i27];
    }

    for (c1_i29 = 0; c1_i29 < 3; c1_i29++) {
      c1_c_F[c1_i29] = c1_F[c1_i29];
    }

    c1_mldivide(chartInstance, c1_f_M, c1_c_F, c1_dv7);
    for (c1_i31 = 0; c1_i31 < 3; c1_i31++) {
      c1_b_b[c1_i31] += c1_dv7[c1_i31];
    }

    c1_h_y = 0.0;
    for (c1_j_k = 1; c1_j_k < 4; c1_j_k++) {
      c1_l_k = c1_j_k - 1;
      c1_h_y += c1_b_a[c1_l_k] * c1_b_b[c1_l_k];
    }

    c1_b_A = c1_h_y;
    c1_c_x = c1_b_A;
    c1_d_x = c1_c_x;
    c1_j_y = c1_d_x / 0.01;
    for (c1_i35 = 0; c1_i35 < 3; c1_i35++) {
      c1_b_a[c1_i35] = c1_z[c1_i35];
    }

    for (c1_i36 = 0; c1_i36 < 3; c1_i36++) {
      c1_b_b[c1_i36] = c1_G[c1_i36];
    }

    c1_l_y = 0.0;
    for (c1_p_k = 1; c1_p_k < 4; c1_p_k++) {
      c1_q_k = c1_p_k - 1;
      c1_l_y += c1_b_a[c1_q_k] * c1_b_b[c1_q_k];
    }

    c1_f_x = c1_l_y;
    c1_h_x = c1_f_x;
    c1_h_x = muDoubleScalarSign(c1_h_x);
    c1_u = c1_j_y * c1_h_x;
    _SFD_SYMBOL_SWITCH(15U, 15U);
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 41);
  c1_b_out = c1_u;
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 42);
  for (c1_i37 = 0; c1_i37 < 3; c1_i37++) {
    c1_b_b[c1_i37] = c1_G[c1_i37];
  }

  c1_d_b = c1_u;
  for (c1_i38 = 0; c1_i38 < 3; c1_i38++) {
    c1_b_u[c1_i38] = c1_b_b[c1_i38] * c1_d_b;
  }

  _SFD_SYMBOL_SWITCH(15U, 20U);
  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 43);
  c1_b_f[0] = 0.0;
  c1_b_f[1] = 0.0;
  c1_b_f[2] = 0.0;
  for (c1_i39 = 0; c1_i39 < 3; c1_i39++) {
    c1_b_f[c1_i39 + 3] = c1_b_u[c1_i39];
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 45);
  if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 1, c1_b_out, 1000.0, -1, 4U,
        c1_b_out > 1000.0))) {
    _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, 46);
    sf_mex_printf("%s =\\n", "ans");
    c1_c_u = 2.0;
    c1_o_y = NULL;
    sf_mex_assign(&c1_o_y, sf_mex_create("y", &c1_c_u, 0, 0U, 0U, 0U, 0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c1_o_y);
  }

  _SFD_EML_CALL(0U, chartInstance->c1_sfEvent, -46);
  _SFD_SYMBOL_SCOPE_POP();
  for (c1_i40 = 0; c1_i40 < 6; c1_i40++) {
    (*chartInstance->c1_f)[c1_i40] = c1_b_f[c1_i40];
  }

  *chartInstance->c1_out = c1_b_out;
  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c1_sfEvent);
}

static void initSimStructsc1_plant(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c1_machineNumber, uint32_T
  c1_chartNumber, uint32_T c1_instanceNumber)
{
  (void)c1_machineNumber;
  (void)c1_chartNumber;
  (void)c1_instanceNumber;
}

static const mxArray *c1_sf_marshallOut(void *chartInstanceVoid, void *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  real_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(real_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static real_T c1_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_b_out, const char_T *c1_identifier)
{
  real_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_out), &c1_thisId);
  sf_mex_destroy(&c1_b_out);
  return c1_y;
}

static real_T c1_b_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  real_T c1_y;
  real_T c1_d18;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_d18, 1, 0, 0U, 0, 0U, 0);
  c1_y = c1_d18;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_out;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_b_out = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_out), &c1_thisId);
  sf_mex_destroy(&c1_b_out);
  *(real_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_b_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i41;
  const mxArray *c1_y = NULL;
  real_T c1_u[6];
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  for (c1_i41 = 0; c1_i41 < 6; c1_i41++) {
    c1_u[c1_i41] = (*(real_T (*)[6])c1_inData)[c1_i41];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 0, 0U, 1U, 0U, 1, 6), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_c_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_b_f, const char_T *c1_identifier, real_T c1_y[6])
{
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_f), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_b_f);
}

static void c1_d_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[6])
{
  real_T c1_dv8[6];
  int32_T c1_i42;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), c1_dv8, 1, 0, 0U, 1, 0U, 1, 6);
  for (c1_i42 = 0; c1_i42 < 6; c1_i42++) {
    c1_y[c1_i42] = c1_dv8[c1_i42];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_f;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y[6];
  int32_T c1_i43;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_b_f = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_f), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_b_f);
  for (c1_i43 = 0; c1_i43 < 6; c1_i43++) {
    (*(real_T (*)[6])c1_outData)[c1_i43] = c1_y[c1_i43];
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_c_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i44;
  const mxArray *c1_y = NULL;
  real_T c1_u[3];
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  for (c1_i44 = 0; c1_i44 < 3; c1_i44++) {
    c1_u[c1_i44] = (*(real_T (*)[3])c1_inData)[c1_i44];
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 0, 0U, 1U, 0U, 1, 3), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_e_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[3])
{
  real_T c1_dv9[3];
  int32_T c1_i45;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), c1_dv9, 1, 0, 0U, 1, 0U, 1, 3);
  for (c1_i45 = 0; c1_i45 < 3; c1_i45++) {
    c1_y[c1_i45] = c1_dv9[c1_i45];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_u;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y[3];
  int32_T c1_i46;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_u = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_u), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_u);
  for (c1_i46 = 0; c1_i46 < 3; c1_i46++) {
    (*(real_T (*)[3])c1_outData)[c1_i46] = c1_y[c1_i46];
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

static const mxArray *c1_d_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_i47;
  int32_T c1_i48;
  const mxArray *c1_y = NULL;
  int32_T c1_i49;
  real_T c1_u[9];
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_i47 = 0;
  for (c1_i48 = 0; c1_i48 < 3; c1_i48++) {
    for (c1_i49 = 0; c1_i49 < 3; c1_i49++) {
      c1_u[c1_i49 + c1_i47] = (*(real_T (*)[9])c1_inData)[c1_i49 + c1_i47];
    }

    c1_i47 += 3;
  }

  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 0, 0U, 1U, 0U, 2, 3, 3), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static void c1_f_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance, const
  mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId, real_T c1_y[9])
{
  real_T c1_dv10[9];
  int32_T c1_i50;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), c1_dv10, 1, 0, 0U, 1, 0U, 2, 3, 3);
  for (c1_i50 = 0; c1_i50 < 9; c1_i50++) {
    c1_y[c1_i50] = c1_dv10[c1_i50];
  }

  sf_mex_destroy(&c1_u);
}

static void c1_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_M;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  real_T c1_y[9];
  int32_T c1_i51;
  int32_T c1_i52;
  int32_T c1_i53;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_M = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_M), &c1_thisId, c1_y);
  sf_mex_destroy(&c1_M);
  c1_i51 = 0;
  for (c1_i52 = 0; c1_i52 < 3; c1_i52++) {
    for (c1_i53 = 0; c1_i53 < 3; c1_i53++) {
      (*(real_T (*)[9])c1_outData)[c1_i53 + c1_i51] = c1_y[c1_i53 + c1_i51];
    }

    c1_i51 += 3;
  }

  sf_mex_destroy(&c1_mxArrayInData);
}

const mxArray *sf_c1_plant_get_eml_resolved_functions_info(void)
{
  const mxArray *c1_nameCaptureInfo = NULL;
  c1_nameCaptureInfo = NULL;
  sf_mex_assign(&c1_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c1_nameCaptureInfo;
}

static real_T c1_cos(SFc1_plantInstanceStruct *chartInstance, real_T c1_x)
{
  real_T c1_b_x;
  c1_b_x = c1_x;
  c1_b_cos(chartInstance, &c1_b_x);
  return c1_b_x;
}

static real_T c1_mpower(SFc1_plantInstanceStruct *chartInstance, real_T c1_a)
{
  real_T c1_c;
  real_T c1_b_a;
  real_T c1_c_a;
  real_T c1_x;
  real_T c1_d_a;
  boolean_T c1_p;
  c1_b_a = c1_a;
  c1_c_a = c1_b_a;
  c1_x = c1_c_a;
  c1_d_a = c1_x;
  c1_c = c1_d_a * c1_d_a;
  c1_p = false;
  if (c1_p) {
    c1_error(chartInstance);
  }

  return c1_c;
}

static void c1_scalarEg(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_dimagree(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_error(SFc1_plantInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_u[31] = { 'C', 'o', 'd', 'e', 'r', ':', 't', 'o', 'o', 'l',
    'b', 'o', 'x', ':', 'p', 'o', 'w', 'e', 'r', '_', 'd', 'o', 'm', 'a', 'i',
    'n', 'E', 'r', 'r', 'o', 'r' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 10, 0U, 1U, 0U, 2, 1, 31), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "error", 0U, 1U, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "message", 1U,
    1U, 14, c1_y));
}

static real_T c1_sin(SFc1_plantInstanceStruct *chartInstance, real_T c1_x)
{
  real_T c1_b_x;
  c1_b_x = c1_x;
  c1_b_sin(chartInstance, &c1_b_x);
  return c1_b_x;
}

static void c1_b_scalarEg(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_c_scalarEg(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_d_scalarEg(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c1_mldivide(SFc1_plantInstanceStruct *chartInstance, real_T c1_A[9],
  real_T c1_B[3], real_T c1_Y[3])
{
  int32_T c1_i54;
  int32_T c1_r1;
  real_T c1_b_A[9];
  int32_T c1_r2;
  int32_T c1_r3;
  real_T c1_x;
  real_T c1_maxval;
  real_T c1_b_x;
  real_T c1_a21;
  real_T c1_c_x;
  real_T c1_d;
  real_T c1_d_x;
  real_T c1_y;
  real_T c1_e_x;
  real_T c1_b_y;
  real_T c1_z;
  real_T c1_f_x;
  real_T c1_c_y;
  real_T c1_g_x;
  real_T c1_d_y;
  real_T c1_b_z;
  real_T c1_h_x;
  real_T c1_b_d;
  real_T c1_i_x;
  real_T c1_c_d;
  int32_T c1_rtemp;
  real_T c1_j_x;
  real_T c1_e_y;
  real_T c1_k_x;
  real_T c1_f_y;
  real_T c1_c_z;
  real_T c1_l_x;
  real_T c1_g_y;
  real_T c1_m_x;
  real_T c1_h_y;
  real_T c1_d_z;
  real_T c1_n_x;
  real_T c1_i_y;
  real_T c1_o_x;
  real_T c1_j_y;
  real_T c1_e_z;
  real_T c1_p_x;
  real_T c1_k_y;
  real_T c1_q_x;
  real_T c1_l_y;
  real_T c1_f_z;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  for (c1_i54 = 0; c1_i54 < 9; c1_i54++) {
    c1_b_A[c1_i54] = c1_A[c1_i54];
  }

  c1_r1 = 0;
  c1_r2 = 1;
  c1_r3 = 2;
  c1_x = c1_b_A[0];
  c1_maxval = c1_abs(chartInstance, c1_x) + c1_abs(chartInstance, 0.0);
  c1_b_x = c1_b_A[1];
  c1_a21 = c1_abs(chartInstance, c1_b_x) + c1_abs(chartInstance, 0.0);
  if (c1_a21 > c1_maxval) {
    c1_maxval = c1_a21;
    c1_r1 = 1;
    c1_r2 = 0;
  }

  c1_c_x = c1_b_A[2];
  c1_d = c1_abs(chartInstance, c1_c_x) + c1_abs(chartInstance, 0.0);
  if (c1_d > c1_maxval) {
    c1_r1 = 2;
    c1_r2 = 1;
    c1_r3 = 0;
  }

  c1_d_x = c1_b_A[c1_r2];
  c1_y = c1_b_A[c1_r1];
  c1_e_x = c1_d_x;
  c1_b_y = c1_y;
  c1_z = c1_e_x / c1_b_y;
  c1_b_A[c1_r2] = c1_z;
  c1_f_x = c1_b_A[c1_r3];
  c1_c_y = c1_b_A[c1_r1];
  c1_g_x = c1_f_x;
  c1_d_y = c1_c_y;
  c1_b_z = c1_g_x / c1_d_y;
  c1_b_A[c1_r3] = c1_b_z;
  c1_b_A[3 + c1_r2] -= c1_b_A[c1_r2] * c1_b_A[3 + c1_r1];
  c1_b_A[3 + c1_r3] -= c1_b_A[c1_r3] * c1_b_A[3 + c1_r1];
  c1_b_A[6 + c1_r2] -= c1_b_A[c1_r2] * c1_b_A[6 + c1_r1];
  c1_b_A[6 + c1_r3] -= c1_b_A[c1_r3] * c1_b_A[6 + c1_r1];
  c1_h_x = c1_b_A[3 + c1_r3];
  c1_b_d = c1_abs(chartInstance, c1_h_x) + c1_abs(chartInstance, 0.0);
  c1_i_x = c1_b_A[3 + c1_r2];
  c1_c_d = c1_abs(chartInstance, c1_i_x) + c1_abs(chartInstance, 0.0);
  if (c1_b_d > c1_c_d) {
    c1_rtemp = c1_r2 + 1;
    c1_r2 = c1_r3;
    c1_r3 = c1_rtemp - 1;
  }

  c1_j_x = c1_b_A[3 + c1_r3];
  c1_e_y = c1_b_A[3 + c1_r2];
  c1_k_x = c1_j_x;
  c1_f_y = c1_e_y;
  c1_c_z = c1_k_x / c1_f_y;
  c1_b_A[3 + c1_r3] = c1_c_z;
  c1_b_A[6 + c1_r3] -= c1_b_A[3 + c1_r3] * c1_b_A[6 + c1_r2];
  guard1 = false;
  guard2 = false;
  if (c1_b_A[c1_r1] == 0.0) {
    guard2 = true;
  } else if (c1_b_A[3 + c1_r2] == 0.0) {
    guard2 = true;
  } else {
    if (c1_b_A[6 + c1_r3] == 0.0) {
      guard1 = true;
    }
  }

  if (guard2 == true) {
    guard1 = true;
  }

  if (guard1 == true) {
    c1_warning(chartInstance);
  }

  c1_Y[0] = c1_B[c1_r1];
  c1_Y[1] = c1_B[c1_r2] - c1_Y[0] * c1_b_A[c1_r2];
  c1_Y[2] = (c1_B[c1_r3] - c1_Y[0] * c1_b_A[c1_r3]) - c1_Y[1] * c1_b_A[3 + c1_r3];
  c1_l_x = c1_Y[2];
  c1_g_y = c1_b_A[6 + c1_r3];
  c1_m_x = c1_l_x;
  c1_h_y = c1_g_y;
  c1_d_z = c1_m_x / c1_h_y;
  c1_Y[2] = c1_d_z;
  c1_Y[0] -= c1_Y[2] * c1_b_A[6 + c1_r1];
  c1_Y[1] -= c1_Y[2] * c1_b_A[6 + c1_r2];
  c1_n_x = c1_Y[1];
  c1_i_y = c1_b_A[3 + c1_r2];
  c1_o_x = c1_n_x;
  c1_j_y = c1_i_y;
  c1_e_z = c1_o_x / c1_j_y;
  c1_Y[1] = c1_e_z;
  c1_Y[0] -= c1_Y[1] * c1_b_A[3 + c1_r1];
  c1_p_x = c1_Y[0];
  c1_k_y = c1_b_A[c1_r1];
  c1_q_x = c1_p_x;
  c1_l_y = c1_k_y;
  c1_f_z = c1_q_x / c1_l_y;
  c1_Y[0] = c1_f_z;
}

static real_T c1_abs(SFc1_plantInstanceStruct *chartInstance, real_T c1_x)
{
  real_T c1_b_x;
  real_T c1_c_x;
  (void)chartInstance;
  c1_b_x = c1_x;
  c1_c_x = c1_b_x;
  return muDoubleScalarAbs(c1_c_x);
}

static void c1_warning(SFc1_plantInstanceStruct *chartInstance)
{
  const mxArray *c1_y = NULL;
  static char_T c1_u[7] = { 'w', 'a', 'r', 'n', 'i', 'n', 'g' };

  const mxArray *c1_b_y = NULL;
  static char_T c1_b_u[7] = { 'm', 'e', 's', 's', 'a', 'g', 'e' };

  const mxArray *c1_c_y = NULL;
  static char_T c1_msgID[27] = { 'C', 'o', 'd', 'e', 'r', ':', 'M', 'A', 'T',
    'L', 'A', 'B', ':', 's', 'i', 'n', 'g', 'u', 'l', 'a', 'r', 'M', 'a', 't',
    'r', 'i', 'x' };

  (void)chartInstance;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", c1_u, 10, 0U, 1U, 0U, 2, 1, 7), false);
  c1_b_y = NULL;
  sf_mex_assign(&c1_b_y, sf_mex_create("y", c1_b_u, 10, 0U, 1U, 0U, 2, 1, 7),
                false);
  c1_c_y = NULL;
  sf_mex_assign(&c1_c_y, sf_mex_create("y", c1_msgID, 10, 0U, 1U, 0U, 2, 1, 27),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 0U, 2U, 14, c1_y, 14,
                    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "feval", 1U,
    2U, 14, c1_b_y, 14, c1_c_y));
}

static const mxArray *c1_e_sf_marshallOut(void *chartInstanceVoid, void
  *c1_inData)
{
  const mxArray *c1_mxArrayOutData = NULL;
  int32_T c1_u;
  const mxArray *c1_y = NULL;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_mxArrayOutData = NULL;
  c1_u = *(int32_T *)c1_inData;
  c1_y = NULL;
  sf_mex_assign(&c1_y, sf_mex_create("y", &c1_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c1_mxArrayOutData, c1_y, false);
  return c1_mxArrayOutData;
}

static int32_T c1_g_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  int32_T c1_y;
  int32_T c1_i55;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_i55, 1, 6, 0U, 0, 0U, 0);
  c1_y = c1_i55;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c1_mxArrayInData, const char_T *c1_varName, void *c1_outData)
{
  const mxArray *c1_b_sfEvent;
  const char_T *c1_identifier;
  emlrtMsgIdentifier c1_thisId;
  int32_T c1_y;
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)chartInstanceVoid;
  c1_b_sfEvent = sf_mex_dup(c1_mxArrayInData);
  c1_identifier = c1_varName;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_sfEvent),
    &c1_thisId);
  sf_mex_destroy(&c1_b_sfEvent);
  *(int32_T *)c1_outData = c1_y;
  sf_mex_destroy(&c1_mxArrayInData);
}

static uint8_T c1_h_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_b_is_active_c1_plant, const char_T *c1_identifier)
{
  uint8_T c1_y;
  emlrtMsgIdentifier c1_thisId;
  c1_thisId.fIdentifier = c1_identifier;
  c1_thisId.fParent = NULL;
  c1_thisId.bParentIsCell = false;
  c1_y = c1_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c1_b_is_active_c1_plant),
    &c1_thisId);
  sf_mex_destroy(&c1_b_is_active_c1_plant);
  return c1_y;
}

static uint8_T c1_i_emlrt_marshallIn(SFc1_plantInstanceStruct *chartInstance,
  const mxArray *c1_u, const emlrtMsgIdentifier *c1_parentId)
{
  uint8_T c1_y;
  uint8_T c1_u0;
  (void)chartInstance;
  sf_mex_import(c1_parentId, sf_mex_dup(c1_u), &c1_u0, 1, 3, 0U, 0, 0U, 0);
  c1_y = c1_u0;
  sf_mex_destroy(&c1_u);
  return c1_y;
}

static void c1_b_cos(SFc1_plantInstanceStruct *chartInstance, real_T *c1_x)
{
  (void)chartInstance;
  *c1_x = muDoubleScalarCos(*c1_x);
}

static void c1_b_sin(SFc1_plantInstanceStruct *chartInstance, real_T *c1_x)
{
  (void)chartInstance;
  *c1_x = muDoubleScalarSin(*c1_x);
}

static void init_dsm_address_info(SFc1_plantInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc1_plantInstanceStruct *chartInstance)
{
  chartInstance->c1_x1 = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    0);
  chartInstance->c1_f = (real_T (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c1_x2 = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    1);
  chartInstance->c1_x3 = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    2);
  chartInstance->c1_x4 = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    3);
  chartInstance->c1_x5 = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    4);
  chartInstance->c1_x6 = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    5);
  chartInstance->c1_out = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c1_plant_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1587143082U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3694381192U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(94159227U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(2457047320U);
}

mxArray* sf_c1_plant_get_post_codegen_info(void);
mxArray *sf_c1_plant_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("B6oUbbK99apMZRmwoMPHbB");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,6,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c1_plant_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c1_plant_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c1_plant_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("pre");
  mxArray *fallbackReason = mxCreateString("hasBreakpoints");
  mxArray *hiddenFallbackType = mxCreateString("none");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c1_plant_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c1_plant_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c1_plant(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x3'type','srcId','name','auxInfo'{{M[1],M[5],T\"f\",},{M[1],M[11],T\"out\",},{M[8],M[0],T\"is_active_c1_plant\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 3, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c1_plant_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_plantInstanceStruct *chartInstance = (SFc1_plantInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _plantMachineNumber_,
           1,
           1,
           1,
           0,
           8,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_plantMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_plantMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _plantMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"x1");
          _SFD_SET_DATA_PROPS(1,1,1,0,"x2");
          _SFD_SET_DATA_PROPS(2,1,1,0,"x3");
          _SFD_SET_DATA_PROPS(3,1,1,0,"x4");
          _SFD_SET_DATA_PROPS(4,1,1,0,"x5");
          _SFD_SET_DATA_PROPS(5,1,1,0,"x6");
          _SFD_SET_DATA_PROPS(6,2,0,1,"f");
          _SFD_SET_DATA_PROPS(7,2,0,1,"out");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,2,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,977);
        _SFD_CV_INIT_EML_IF(0,1,0,744,760,801,861);
        _SFD_CV_INIT_EML_IF(0,1,1,920,933,-1,957);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,748,759,-1,4);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,924,932,-1,4);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 6U;
          _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c1_b_sf_marshallOut,(MexInFcnForType)
            c1_b_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c1_sf_marshallOut,(MexInFcnForType)c1_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _plantMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc1_plantInstanceStruct *chartInstance = (SFc1_plantInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, chartInstance->c1_x1);
        _SFD_SET_DATA_VALUE_PTR(6U, *chartInstance->c1_f);
        _SFD_SET_DATA_VALUE_PTR(1U, chartInstance->c1_x2);
        _SFD_SET_DATA_VALUE_PTR(2U, chartInstance->c1_x3);
        _SFD_SET_DATA_VALUE_PTR(3U, chartInstance->c1_x4);
        _SFD_SET_DATA_VALUE_PTR(4U, chartInstance->c1_x5);
        _SFD_SET_DATA_VALUE_PTR(5U, chartInstance->c1_x6);
        _SFD_SET_DATA_VALUE_PTR(7U, chartInstance->c1_out);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sxxelTfzDkYotD0fSAoG9dH";
}

static void sf_opaque_initialize_c1_plant(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc1_plantInstanceStruct*) chartInstanceVar)->S,0);
  initialize_params_c1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
  initialize_c1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c1_plant(void *chartInstanceVar)
{
  enable_c1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c1_plant(void *chartInstanceVar)
{
  disable_c1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c1_plant(void *chartInstanceVar)
{
  sf_gateway_c1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c1_plant(SimStruct* S)
{
  return get_sim_state_c1_plant((SFc1_plantInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c1_plant(SimStruct* S, const mxArray *st)
{
  set_sim_state_c1_plant((SFc1_plantInstanceStruct*)sf_get_chart_instance_ptr(S),
    st);
}

static void sf_opaque_terminate_c1_plant(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc1_plantInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_plant_optimization_info();
    }

    finalize_c1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc1_plant((SFc1_plantInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c1_plant(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c1_plant((SFc1_plantInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c1_plant(SimStruct *S)
{
  /* Set overwritable ports for inplace optimization */
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_plant_optimization_info(sim_mode_is_rtw_gen(S),
      sim_mode_is_modelref_sim(S), sim_mode_is_external(S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,1);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,1,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 1);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,1);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 5, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,1,6);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,1,2);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=2; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 6; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,1);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2885568917U));
  ssSetChecksum1(S,(2791161714U));
  ssSetChecksum2(S,(3820637792U));
  ssSetChecksum3(S,(3709088482U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c1_plant(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c1_plant(SimStruct *S)
{
  SFc1_plantInstanceStruct *chartInstance;
  chartInstance = (SFc1_plantInstanceStruct *)utMalloc(sizeof
    (SFc1_plantInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc1_plantInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c1_plant;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c1_plant;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c1_plant;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c1_plant;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c1_plant;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c1_plant;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c1_plant;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c1_plant;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c1_plant;
  chartInstance->chartInfo.mdlStart = mdlStart_c1_plant;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c1_plant;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c1_plant(chartInstance);
}

void c1_plant_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c1_plant(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c1_plant(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c1_plant(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c1_plant_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
