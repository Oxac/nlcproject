#ifndef __c1_plant_h__
#define __c1_plant_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc1_plantInstanceStruct
#define typedef_SFc1_plantInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  boolean_T c1_doneDoubleBufferReInit;
  uint8_T c1_is_active_c1_plant;
  real_T *c1_x1;
  real_T (*c1_f)[6];
  real_T *c1_x2;
  real_T *c1_x3;
  real_T *c1_x4;
  real_T *c1_x5;
  real_T *c1_x6;
  real_T *c1_out;
} SFc1_plantInstanceStruct;

#endif                                 /*typedef_SFc1_plantInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_plant_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_plant_get_check_sum(mxArray *plhs[]);
extern void c1_plant_method_dispatcher(SimStruct *S, int_T method, void *data);

#endif
