function ctrl = fcn(x1,x2,x3,x4,x5,x6)

m0 = 1;
m1 = .5;
m2 = .5;
l1 = 1;
l2 = 1;
g=9.82;
Er = 1.5;
kD = 5;
k = 20;
kP = 12;

M = [(m0 + m1 + m2) (m1+m2)*l1*cos(x2) m2*l2*cos(x3)
    (m1+m2)*l1*cos(x2) 1^2*(m1+m2) m2*l1*l2*cos(x2-x3)
    m2*l2*cos(x3) m2*l1*l2*cos(x2-x3) m2*l2^2];

F = [(m1+m2)*l1*x5^2*sin(x2)+m2*l2*x6^2*sin(x3)
    (m1+m2)*l1*g*sin(x2)-m2*l1*l2*x6^2*sin(x2-x3)
    m2*l2*g*sin(x3)+m2*l1*l2*x5^2*sin(x2-x3)];

P =  l1*(m1+m2)*cos(x2)+l2*m2*cos(x3);
E = [x4, x5, x6]*M*[x4, x5, x6]' + P;

B = [1 0 0]';

ctrl = (kD*B'*(M\F)- kP*x1 - k*x4)/(E - Er + kD*B'*(M\B));
end